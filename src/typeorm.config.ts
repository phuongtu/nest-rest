import { TypeOrmModuleOptions } from '@nestjs/typeorm';
// import { AuthTokenEntity } from './modules/auth/entities/auth.entity';
// import { MediaEntity } from './modules/media/entities/media.entity';
import { User } from 'src/modules/users/entities/users.entity';
// import { Sample } from './sample/entities/sample.entity';

export const typeORMConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  port: parseInt(process.env.POSTGRES_PORT || '5432', 10),
  host: process.env.POSTGRES_HOST,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DATABASE,
  synchronize: process.env.POSTGRES_SYNC === 'true',
  logging: process.env.POSTGRES_LOGGING === 'true',
  entities: [User],
};
