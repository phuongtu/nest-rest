export const USER_NOT_FOUND = 'User not found';
export const USER_EXISTS = 'User already exists';
