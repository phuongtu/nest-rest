import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { RabbitController } from './rabbit.controller';
import { RabbitService } from './rabbit.service';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'HELLO_SERVICE_RABBIT',
        transport: Transport.RMQ,
        options: {
          urls: ['amqp://localhost:5672'],
          queue: 'cats_queue',
          queueOptions: {
            durable: false,
          },
        },
      },
    ]),
  ],
  controllers: [RabbitController],
  providers: [RabbitService],
})
export class RabbitModule {}
