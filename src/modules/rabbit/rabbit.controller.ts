import { Controller, Get, Inject } from '@nestjs/common';
import {
  ClientProxy,
  MessagePattern,
  Payload,
  Ctx,
  RmqContext,
} from '@nestjs/microservices';

@Controller('rabbit')
export class RabbitController {
  constructor(
    @Inject('HELLO_SERVICE_RABBIT') private readonly client: ClientProxy,
  ) {}

  @Get()
  getHello() {
    this.client.emit<any>('message_printed', 'Hello World 111111cccsssaa');
    return 'Hello World printed';
  }

  @MessagePattern('message_printed')
  public async execute(@Payload() data: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMessage = context.getMessage();

    console.log('data:', data);
    // await this.appService.mySuperLongProcessOfUser(data);

    channel.ack(originalMessage);
  }
}
