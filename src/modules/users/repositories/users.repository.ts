import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../entities/users.entity';

import { RepositoryBase } from 'src/commons/bases/repositories';
import { Injectable } from '@nestjs/common';

@Injectable()
export class UserRepository extends RepositoryBase<User> {
  constructor(@InjectRepository(User) repository: Repository<User>) {
    super(repository.target, repository.manager, repository.queryRunner);
  }
}
