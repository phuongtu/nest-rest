import {
  Body,
  Controller,
  Get,
  Post,
  Req,
  Request,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AppRoles } from 'src/commons/enum';
import { Roles } from 'src/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/guards/auth.guard';
import { RolesGuard } from 'src/guards/roles.guard';
import { TransformInterceptor } from 'src/interceptors/transform.interceptor';
import { UserRegisterDto } from './dto/new-users.input';
import { UsersService } from './users.service';

@Controller('users')
@ApiTags('posts')
@UseInterceptors(new TransformInterceptor())
export class UsersController {
  constructor(private readonly userService: UsersService) {}
  @Get()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(AppRoles.ADMIN)
  findOneUser(@Req() request) {
    const { user } = request;
    return {
      id: user.id,
      email: user.email,
      username: user.username,
    };
  }

  @Post()
  async register(@Body() userRegisterDto: UserRegisterDto) {
    return this.userService.saveUser(userRegisterDto);
  }
}
