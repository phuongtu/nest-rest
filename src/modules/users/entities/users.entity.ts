import { Entity, Column, DeepPartial, BaseEntity } from 'typeorm';
import { snowflake } from 'src/commons/common';
import { AppRoles } from 'src/commons/enum';

@Entity({ name: 'users' })
export class User extends BaseEntity {
  @Column('bigint', {
    primary: true,
    unsigned: true,
  })
  id: string;

  @Column({ length: 50, unique: true })
  email: string;

  @Column({ length: 255 })
  username: string;

  @Column()
  password: string;

  @Column()
  passwordSalt: string;

  @Column({ nullable: true, name: 'first_name' })
  firstName: string;

  @Column({ nullable: true, name: 'last_name' })
  lastName: string;

  @Column('int', { nullable: true })
  age: number;

  @Column({
    default: false,
    name: 'is_active',
  })
  isActive: boolean;

  @Column({
    nullable: true,
    type: 'text',
    enum: AppRoles,
    array: true,
  })
  roles?: string[];

  constructor(partial: DeepPartial<User>) {
    super();
    Object.assign(this, { id: snowflake.nextId(), ...partial });
  }
}
