import { ConflictException, Injectable } from '@nestjs/common';
import bcrypt from 'bcryptjs';
import { USER_EXISTS } from 'src/commons/errors/message.error';
import { UserRegisterDto } from './dto/new-users.input';
import { User } from './entities/users.entity';
import { UserRepository } from './repositories/users.repository';

@Injectable()
export class UsersService {
  constructor(private readonly userRepository: UserRepository) {}

  async saveUser(req: UserRegisterDto): Promise<User | undefined> {
    const user = await this.findByEmail(req.email);
    if (user) {
      throw new ConflictException(USER_EXISTS);
    }

    const salt = bcrypt.genSaltSync(10);
    req.roles = ['BASE', 'ADMIN'];
    req.password = bcrypt.hashSync(req.password ?? '', salt);
    req.passwordSalt = salt;
    const userData = this.userRepository.create(req);
    return this.userRepository.save(userData);
  }

  findByEmail(email: string): Promise<User | undefined> {
    return this.userRepository.findOne({
      where: { email: email },
    });
  }
}
