import { Controller, Get, Inject, OnModuleInit } from '@nestjs/common';
import {
  ClientKafka,
  MessagePattern,
  Payload,
  Ctx,
  KafkaContext,
} from '@nestjs/microservices';

@Controller('kafka')
export class KafkaController implements OnModuleInit {
  //   constructor(
  //     @Inject('HELLO_SERVICE_KAFKA') private readonly client: ClientKafka,
  //   ) {}

  async onModuleInit() {
    //     this.client.subscribeToResponseOf('message_printed');
    //     await this.client.connect();
  }

  //   @Get()
  //   getHello() {
  //     this.client.emit<any>(
  //       'message_printed',
  //       'Hello World 111111cccsssaa kafka',
  //     );
  //     return 'Hello World printed';
  //   }

  //   @MessagePattern('message_printed')
  //   public async execute(@Payload() data: any, @Ctx() context: KafkaContext) {
  //     console.log('data:', data);
  //   }
}
