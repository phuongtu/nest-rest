import { Controller, Get, Inject } from '@nestjs/common';
import {
  ClientProxy,
  MessagePattern,
  Payload,
  Ctx,
  RedisContext,
} from '@nestjs/microservices';

@Controller('redis')
export class RedisController {
  constructor(
    @Inject('HELLO_SERVICE_REDIS') private readonly client: ClientProxy,
  ) {}

  @Get()
  getHello() {
    this.client.emit<any>(
      'message_printed',
      'Hello World 111111cccsssaa redis',
    );
    return 'Hello World printed';
  }

  @MessagePattern('message_printed')
  public async execute(@Payload() data: any, @Ctx() context: RedisContext) {
    // const channel = context.getChannel();

    console.log('data:', data);
  }
}
