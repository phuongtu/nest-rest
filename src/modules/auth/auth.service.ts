import { Injectable, NotFoundException } from '@nestjs/common';
import { UserLoginDto } from './dto/login.dto';
import { JwtService } from '@nestjs/jwt';
import bcrypt from 'bcryptjs';
import { UsersService } from '../users/users.service';
import { USER_NOT_FOUND } from 'src/commons/errors/message.error';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async login(reqBody: UserLoginDto) {
    const user = await this.userService.findByEmail(reqBody.email);
    if (!user) {
      throw new NotFoundException(USER_NOT_FOUND);
    }
    const check = bcrypt.compareSync(reqBody.password, user.password);
    if (!check) {
      throw new NotFoundException(USER_NOT_FOUND);
    }
    const accessToken = this.jwtService.sign(
      {
        username: user.username,
        email: user.email,
        sub: user.id,
      },
      {
        expiresIn: `30 days`,
      },
    );
    return {
      id: user.id,
      email: user.email,
      accessToken,
    };
  }
}
