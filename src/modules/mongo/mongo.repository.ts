import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { MongoRepositoryBase } from 'src/commons/bases/mongo.reposotories';
import { Users } from './schemas/users.schema';

export class MongoRepository extends MongoRepositoryBase {
  constructor(
    @InjectModel(Users.name) private readonly usersSchema: Model<Users>,
  ) {
    super(usersSchema);
  }
}
